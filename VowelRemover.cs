﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disemvoweler
{
    class VowelRemover
    {
        private string Vowels
        {
            get;
            set;

        }
        public VowelRemover(string vowels)
        {
            this.Vowels = vowels;
        }


        /// <summary>
        /// Will return a list of Strings the first will be the input string without vowels.
        /// 
        /// </summary>
        /// <param name="toLoseVowels">input string</param>
        /// <returns>First items in list is string with no vowels, 
        /// second item will be vowels in order.</returns>
        public List<String> removeAllVowelsInOrder(string toLoseVowels){
            List<String> answers = new List<string>();

            char[] toLoseVowelsArray = toLoseVowels.ToCharArray();

            StringBuilder noMoreVowels = new StringBuilder();
            StringBuilder origVowels = new StringBuilder();

            foreach(char ch in toLoseVowelsArray)
            {
                if (this.Vowels.Contains(ch))
                {
                    origVowels.Append(ch);
                }
                else if(ch != ' ')
                {
                    noMoreVowels.Append(ch);
                }
            }

            answers.Add(noMoreVowels.ToString());
            answers.Add(origVowels.ToString());

            if (answers.Count != 2)
            {
                throw new Exception("Bad Answer");
            }
            return answers;
        }
    }
}
