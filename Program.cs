﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disemvoweler
{
    class Program
    {
        /*Disemvoweling means removing the vowels from text. (For this challenge, the letters 
         * a, e, i, o, and u are considered vowels, and the letter y is not.) The idea is to 
         * make text difficult but not impossible to read, for when somebody posts something 
         * so idiotic you want people who are reading it to get extra frustrated.
         * To make things even harder to read, we'll remove spaces too. For example, this string:
         * two drums and a cymbal fall off a cliff
         * can be disemvoweled to get:
         * * twdrmsndcymblfllffclff
         * We also want to keep the vowels we removed around (in their original order), which in this case is:
         * ouaaaaoai*/
        static void Main(string[] args)
        {
            string input = "all those who believe in psychokinesis raise my hand";

            VowelRemover remover = new VowelRemover("aeiou");
            List<String> ans =  remover.removeAllVowelsInOrder(input);

            foreach (String print in ans)
            {
                System.Console.WriteLine(print);
            }

            System.Console.ReadLine();

        }
    }
}
